#!/usr/bin/ruby

class Common
    def self.[] pkg
        if cache[pkg] then
            #log 'already built'
            return
        end

        cache[pkg] = true

        begin
            result = invoke pkg

            if result then
                raise "BUILD_ERROR"
            end
        rescue => s
            raise "\n  #{class_name()[0]} #{pkg} -> #{s}"
        end

        result
    end

    def self.precache (vals)
        vals.each { |v| cache[v] = true }
    end

    def self.summary
        log cache.size,cache.keys.sort.join(', ')
    end

    private

    def self.log pkg,msg
        puts "#{class_name} #{pkg}: #{msg}"
        msg
    end

    def self.pkgdir pkg
        "packages/#{pkg}/trunk"
    end
end

class Builder < Common
    private

    @@CACHE = {}
    def self.cache
        @@CACHE
    end

    def self.class_name
        'BUILD'
    end

    def self.invoke pkg
        return :TEST if pkg == 'libpng'

        if not Dir.glob("#{pkgdir pkg}/*.tar.xz").empty? then
            log pkg,:BUILT
            return false
        end

        lines = IO.popen(['bash', '-c', %Q{
            function dump {
                for i; do echo $i; done
            }
            NAME=#{pkgdir pkg}/PKGBUILD
            [ -f $NAME ] || exit 1
            source $NAME
            dump ${depends[@]}
            echo
            dump ${makedepends[@]}
        }]).readlines

        Process.wait

        if $? != 0 then
            return log pkg,:PKGBUILD_ERR
        end

        lines.map! { |x| x.sub /((>|<|=).*)?\n?$/, '' }

        sep = lines.index ''
        deps  = lines[0...sep]
        mdeps = lines[sep+1...lines.size]

        mdeps.each { |p|
            Installer[p]
        }
        deps.each { |p|
            Builder[p]
        }

        # build this package here

        Process.wait Process.spawn("pacman -Qi #{pkg} | grep Descr")

        if true then
            Process.wait Process.spawn('makepkg -d', :chdir => pkgdir(pkg) )

            if $? != 0 then
                log pkg,"makepkg exited with #{$?}"

                return :MAKEPKG_ERR
            end
        end

        log pkg,:OK
        false
    end
end

class Installer < Common
    private

    @@CACHE = {}
    def self.cache
        @@CACHE
    end

    def self.class_name
        'INSTL'
    end

    def self.invoke pkg
        result = Builder[pkg]

        if ! result then
            # install the package here

            log pkg,:OK
        end

        result
    end
end

Builder.precache %w[
    binutils tzdata glibc gcc-libs sh db bash util-linux-ng
    ncurses readline m4 flex pam openssl
    zlib gzip bzip2

    libxslt libxml2

    libxcb libx11 libxfont xcursor-themes

    mesa libgl ati-dri intel-dri unichrome-dri mach64-dri mga-dri r128-dri savage-dri sis-dri tdfx-dri
]

Installer.precache %w[
    gcc python
    gdbm patch

    xorg-apps
    intltool
]

Builder['xorg-server']

Builder.summary
Installer.summary

