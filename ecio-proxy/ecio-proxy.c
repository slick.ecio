
/*
 *  Ecio proxy module 2011 (c) Yury Makarevich
 *
 *  We wrap register_chrdev/unregister_chrdev, copy_from_user
 */

/*
 *  Kernel config:  disable SMP, disable module versioning.
 *
 *  EcIo.ko config: patch the appropriate symbols, rename per_cpu__current_task to current_task (remove?),
 *                  remove spin_lock related code.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>

#include <asm/uaccess.h>

/*
    The following structure was copied from 2.6.22 kernel
 */

struct legacy_file_operations {
	struct module *owner;
	loff_t (*llseek) (struct file *, loff_t, int);
	ssize_t (*read) (struct file *, char __user *, size_t, loff_t *);
	ssize_t (*write) (struct file *, const char __user *, size_t, loff_t *);
	ssize_t (*aio_read) (struct kiocb *, const struct iovec *, unsigned long, loff_t);
	ssize_t (*aio_write) (struct kiocb *, const struct iovec *, unsigned long, loff_t);
	int (*readdir) (struct file *, void *, filldir_t);
	unsigned int (*poll) (struct file *, struct poll_table_struct *);
	int (*ioctl) (struct inode *, struct file *, unsigned int, unsigned long);
	long (*unlocked_ioctl) (struct file *, unsigned int, unsigned long);
	long (*compat_ioctl) (struct file *, unsigned int, unsigned long);
	int (*mmap) (struct file *, struct vm_area_struct *);
	int (*open) (struct inode *, struct file *);
	int (*flush) (struct file *, fl_owner_t id);
	int (*release) (struct inode *, struct file *);
	int (*fsync) (struct file *, struct dentry *, int datasync);
	int (*aio_fsync) (struct kiocb *, int datasync);
	int (*fasync) (int, struct file *, int);
	int (*lock) (struct file *, int, struct file_lock *);
	ssize_t (*sendfile) (struct file *, loff_t *, size_t, read_actor_t, void *);
	ssize_t (*sendpage) (struct file *, struct page *, int, size_t, loff_t *, int);
	unsigned long (*get_unmapped_area)(struct file *, unsigned long, unsigned long, unsigned long, unsigned long);
	int (*check_flags)(int);
	int (*dir_notify)(struct file *filp, unsigned long arg);
	int (*flock) (struct file *, int, struct file_lock *);
	ssize_t (*splice_write)(struct pipe_inode_info *, struct file *, loff_t *, size_t, unsigned int);
	ssize_t (*splice_read)(struct file *, loff_t *, struct pipe_inode_info *, size_t, unsigned int);
};

static long proxy_ioctl(struct file *file, unsigned int cmd, unsigned long arg);

static const struct legacy_file_operations     *legacy_fops = NULL;
static struct file_operations             proxy_fops = {
    .unlocked_ioctl     = proxy_ioctl,
};


int register_ecio(unsigned int major, const char *name,
		    const struct legacy_file_operations *lfops)
{
    // wrap file_operations here
    legacy_fops = lfops;

    proxy_fops.open     = legacy_fops->open;
    proxy_fops.read     = legacy_fops->read;
    proxy_fops.write    = legacy_fops->write;
    proxy_fops.release  = legacy_fops->release;

    return register_chrdev(major, name, &proxy_fops);
}

int unregister_ecio(unsigned int major, const char *name)
{
    unregister_chrdev(major, name);
    return 0;
}

/////////////////////////////////////////////////////////////////

static spinlock_t   proxy_ioctl_lock;

static long proxy_ioctl(struct file *file, unsigned int cmd, unsigned long arg) {
    long  result;

    printk(KERN_INFO "ecio-proxy ioctl %d\n", cmd);

    spin_lock(&proxy_ioctl_lock);
    result = legacy_fops->ioctl(file->f_path.dentry->d_inode, file, cmd, arg);
    spin_unlock(&proxy_ioctl_lock);

    return result;
}

unsigned long copy_from_ecio(void *to, const void __user *from, unsigned long n)
{
    return copy_from_user(to, from, n);
}

void _spin_ecio(spinlock_t *lock) {
}

/////////////////////////////////////////////////////////////////

int init_module (void) {
    printk(KERN_INFO "ecio-proxy inserted\n");
    return 0;
}

void cleanup_module (void) {
    printk(KERN_INFO "ecio-proxy removed\n");
}

EXPORT_SYMBOL(register_ecio);
EXPORT_SYMBOL(unregister_ecio);
EXPORT_SYMBOL(copy_from_ecio);
EXPORT_SYMBOL(_spin_ecio);
